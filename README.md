# Автоматическое развертывание вэб-приложения
https://api.postman.com/collections/18788525-be8ca0d2-310a-485d-9f42-700584d85035?access_key=PMAT-01HJZVVWD029RV6Y0F1E5WA2BQ
## REST API
## Getting started

Для работы с REST API необходимо авторизоваться.
Для проверки можно использовать профиль администратора:
Username: admin
Password: admin123

## Method GET

### Получение списка пользователей
```
GET  http://127.0.0.1:8000/users/
```
Пример ответа:
```
{
    "count": 1,
    "next": null,
    "previous": null,
    "results": [
        {
            "url": "http://127.0.0.1:8000/users/1/",
            "username": "admin",
            "email": "admin@example.com",
            "groups": []
        }
    ]
}
```

### Получение списка групп пользователей
```
GET  http://127.0.0.1:8000/groups/
```
Пример ответа:
```
{
    "count": 0,
    "next": null,
    "previous": null,
    "results": []
}
```

### Получение информации об отдельном пользователе
```
GET  http://127.0.0.1:8000/users/{id}/
```

Пример запроса с id = 1
```
GET  http://127.0.0.1:8000/users/1/
```
Пример ответа:
```
{
    "url": "http://127.0.0.1:8000/users/1/",
    "username": "admin",
    "email": "admin@example.com",
    "groups": []
}

```

## Method POST

### Добавление нового пользователя
```
POST  http://127.0.0.1:8000/users/
```

Тело запроса
```
username: name
email: name@example.com
```

Пример ответа:
```
{
    "url": "http://127.0.0.1:8000/users/3/",
    "username": "Danil",
    "email": "danil@example.com",
    "groups": []
}
```